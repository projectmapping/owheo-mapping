#! /usr/bin/env bash

##########################################################################################
# This script clean GeoServer default layers, create Owheo layers and style #
##########################################################################################

#add workspace
curl -u admin:geoserver -XPOST -H "Content-type: text/xml" -d "<workspace><name>Owheo</name></workspace>" http://localhost:8080/geoserver/rest/workspaces

#add stores for Owheo building and Nodes network with the connection to corresponding DBs
curl -u admin:geoserver -XPOST -T /vagrant/stores/storeOwheo.xml -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/workspaces/Owheo/datastores
curl -u admin:geoserver -XPOST -T /vagrant/stores/storeNodes.xml -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/workspaces/Owheo/datastores

#add the polygon layer
curl -u admin:geoserver -XPOST -H "Content-type: text/xml" -d "<featureType><name>owheo</name></featureType>" http://localhost:8080/geoserver/rest/workspaces/Owheo/datastores/Owheo/featuretypes
curl -u admin:geoserver -XPOST -H "Content-type: text/xml" -d "<featureType><name>nodes</name></featureType>" http://localhost:8080/geoserver/rest/workspaces/Owheo/datastores/Nodes/featuretypes

#Adding styles for each layer
#Only the OwheoStyle.sld can be added with curl
#Styles for data layers are added by copying files into geoserver repository on server
curl -u admin:geoserver -XPOST -H "Content-type: text/xml" -d "<style><name>OwheoStyle</name><filename>OwheoStyle.sld</filename></style>" http://localhost:8080/geoserver/rest/styles
curl -u admin:geoserver -XPUT -H "Content-type: application/vnd.ogc.sld+xml" -d @/vagrant/stores/OwheoStyle.sld http://localhost:8080/geoserver/rest/styles/OwheoStyle
sudo cp /vagrant/stores/Node* /var/lib/tomcat7/webapps/geoserver/data/styles/
sudo chmod 777 -R /var/lib/tomcat7/webapps/geoserver/
sudo /etc/init.d/tomcat7 restart
