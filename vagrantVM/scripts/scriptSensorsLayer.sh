#!/bin/bash

#####################################################################
# This script update GeoServer to add sensors layer and grouplayers #
#####################################################################

#create folders for gifs app
sudo mkdir /var/www/gifs/images
sudo mkdir /var/www/gifs/gif
sudo mkdir /var/www/gifs/data
sudo chmod 777 -R /var/www/gifs

#adding grouplayers (here cause need data inside sensors table or it will fail) - restart to enable wps service
sudo /etc/init.d/tomcat7 restart
sudo /etc/init.d/apache2 restart
curl -u admin:geoserver -XPOST -d @/vagrant/stores/LayerGroupTemperatureNow.xml -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/layergroups
curl -u admin:geoserver -XPOST -d @/vagrant/stores/LayerGroupHumidityNow.xml -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/layergroups
curl -u admin:geoserver -XPOST -d @/vagrant/stores/LayerGroupLightNow.xml -H "Content-type: text/xml" http://localhost:8080/geoserver/rest/layergroups

#get the legends to merge into pngs and gifs
sudo wget -O /var/www/gifs/images/tempLegend.png -A png "http://localhost:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&&STYLE=NodeStyle&FORMAT=image/png&WIDTH=50&HEIGHT=20&LEGEND_OPTIONS=forceRule:true&LAYER=Owheo:nodes&transparent=true"
sudo wget -O /var/www/gifs/images/lgtLegend.png -A png "http://localhost:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&&STYLE=NodeLightStyle&FORMAT=image/png&WIDTH=50&HEIGHT=20&LEGEND_OPTIONS=forceRule:true&LAYER=Owheo:nodes&transparent=true"
sudo wget -O /var/www/gifs/images/humLegend.png -A png "http://localhost:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&&STYLE=NodeHumidityStyle&FORMAT=image/png&WIDTH=50&HEIGHT=20&LEGEND_OPTIONS=forceRule:true&LAYER=Owheo:nodes&transparent=true"

#call script to create gifs
sudo sh /vagrant/scripts/scriptGif.sh
