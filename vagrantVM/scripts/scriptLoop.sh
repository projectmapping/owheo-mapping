#!/bin/bash

echo "VM READY"

#infinite loop
while :
do
  #if the file does not exist -> wait and check again
  if [ ! -f /vagrant/sensors.csv ]
  then
    exists=0;
    sleep 0.1
  #if the file exists
  else
    #if it's the first loop $exists is not set
    if [[ ! -z "$exists" ]]; then
      #if the file has been created
      if [ $exists == 0 ]; then
        exists=1;
        #wait 6 seconds for more data added in file
	echo "NEW DATA...WAITING..."
	sleep 6
        #call script to insert data
        sudo bash /vagrant/scripts/scriptInsert.sh
      fi;
    fi;
  fi;
done

