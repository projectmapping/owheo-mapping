#!/usr/bin/env bash

#at first time, if the file does not exist : display cute kitten
if [ ! -f /vagrant/sensors.csv ]; then
cp /var/www/gifs/error/404.png /var/www/gifs/gif/gifTemp.gif
cp /var/www/gifs/error/404.png /var/www/gifs/gif/gifLgt.gif
cp /var/www/gifs/error/404.png /var/www/gifs/gif/gifHum.gif

#when the script is called by scriptLoop.sh
else
#clean any gif
sudo rm /var/www/gifs/gif/*

#get the max timestamp to identify every layer
psql -U postgres -d Nodes -c "SELECT MAX(timestamp) FROM Nodes;" > tmstp.txt

#extract only the timestamp data (delete column name and ----- lines)
sed '1,2d' tmstp.txt > tmstp2.txt
sed '2d' tmstp2.txt > tmstp.txt

#this is the timestamp taken as a filename
filename=$(cat tmstp.txt)

#create distinct files for temperature, humidity and light
name1=$(echo $filename | cut -f1 -d' ')
name2=$(echo $filename | cut -f2 -d' ')
nameTemp=$(echo 'Temp['$name1'['$name2)
nameLgt=$(echo 'Lgt['$name1'['$name2)
nameHum=$(echo 'Hum['$name1'['$name2)

#get layers as png from geoserver and save them with data type + timestamp
sudo wget -O /var/www/gifs/images/$nameTemp.png -A png "http://localhost:8080/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=TemperatureNow&CQL_FILTER=level+NOT+IN+(+0+,+2+)&styles=&bbox=1.8981907E7,-5759124.0,1.8982050E7,-5758997.0&width=512&height=454&srs=EPSG:900913&format=image/png"
sudo wget -O /var/www/gifs/images/$nameLgt.png -A png "http://localhost:8080/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=LightNow&CQL_FILTER=level+NOT+IN+(+0+,+2+)&styles=&bbox=1.8981907E7,-5759124.0,1.8982050E7,-5758997.0&width=512&height=454&srs=EPSG:900913&format=image/png"
sudo wget -O /var/www/gifs/images/$nameHum.png -A png "http://localhost:8080/geoserver/wms?service=WMS&version=1.1.0&request=GetMap&layers=HumidityNow&CQL_FILTER=level+NOT+IN+(+0+,+2+)&styles=&bbox=1.8981907E7,-5759124.0,1.8982050E7,-5758997.0&width=512&height=454&srs=EPSG:900913&format=image/png"

#init file with selectBox to select a specific layer to display
echo "<html>\n<body>\n<form action=\"\" method=\"POST\">\n<?php\nif (isset(\$_POST['lgttime'])) {\n\$lgttime = \$_POST['lgttime'];\necho '<input type=\"hidden\" name=\"lgttime\" value=\"'.\$lgttime.'\">';\n}\nif (isset(\$_POST['humtime'])) {\n\$humtime = \$_POST['humtime'];\necho '<input type=\"hidden\" name=\"humtime\" value=\"'.\$humtime.'\">';\n}\n?>\n<select id=\"temptime\" name=\"temptime\" OnChange=\"this.form.submit()\">\n<option value=\"choose\">Choose</option>\n<option value=\"GIF\">GIF</option>" > /var/www/gifs/TempHist.php
echo "<html>\n<body>\n<form action=\"\" method=\"POST\">\n<?php\nif (isset(\$_POST['temptime'])) {\n\$temptime = \$_POST['temptime'];\necho '<input type=\"hidden\" name=\"temptime\" value=\"'.\$temptime.'\">';\n}\nif (isset(\$_POST['humtime'])) {\n\$humtime = \$_POST['humtime'];\necho '<input type=\"hidden\" name=\"humtime\" value=\"'.\$humtime.'\">';\n}\n?>\n<select id=\"lgttime\" name=\"lgttime\" OnChange=\"this.form.submit()\">\n<option value=\"choose\">Choose</option>\n<option value=\"GIF\">GIF</option>" > /var/www/gifs/LgtHist.php
echo "<html>\n<body>\n<form action=\"\" method=\"POST\">\n<?php\nif (isset(\$_POST['temptime'])) {\n\$temptime = \$_POST['temptime'];\necho '<input type=\"hidden\" name=\"temptime\" value=\"'.\$temptime.'\">';\n}\nif (isset(\$_POST['lgttime'])) {\n\$lgttime = \$_POST['lgttime'];\necho '<input type=\"hidden\" name=\"lgttime\" value=\"'.\$lgttime.'\">';\n}\n?>\n<select id=\"humtime\" name=\"humtime\" OnChange=\"this.form.submit()\">\n<option value=\"choose\">Choose</option>\n<option value=\"GIF\">GIF</option>" > /var/www/gifs/HumHist.php

#init php scripts for gifs / add encoder class
echo "<?php\ninclude_once('GIFEncoder.class.php');" > /var/www/gifs/gifTemp.php
echo "<?php\ninclude_once('GIFEncoder.class.php');" > /var/www/gifs/gifLgt.php
echo "<?php\ninclude_once('GIFEncoder.class.php');" > /var/www/gifs/gifHum.php

#make a list of png layers saved into images folders (usually 3 times of number of update done on DBs)
ls /var/www/gifs/images > listImg.txt

#init newline for the while loop
oldIFS=$IFS
IFS=$'\n'

while read line
do
#cut the name to have the timestamp
  img1=$(echo $line | cut -f1 -d' ')
  img2=$(echo $line | cut -f2 -d' ')
  type=$(echo $img1 | cut -f1 -d'[')
  date1=$(echo $img1 | cut -f2 -d'[')
  date2=$(echo $img1 | cut -f3 -d'[')
  date21=$(echo $date2 | cut -f1 -d'.')

#for temperature data
  if [ "$type" = "Temp" ]; then
#add an option into selectBox for this timestamp
    echo '<option value="'$date1' '$date21'">'$date1' '$date21'</option>' >> /var/www/gifs/TempHist.php

#write script to create gif image for each pngs
    echo "\$text = \"DateTime : "$date1" at "$date21"\";\n" >> /var/www/gifs/gifTemp.php
#layer captured
    echo "\$image = imagecreatefrompng('images/"$img1"n"$img2"');" >> /var/www/gifs/gifTemp.php
#legend of this layer specified in NodeStyle.sld
    echo "\$image12 = imagecreatefrompng('images/tempLegend.png');" >> /var/www/gifs/gifTemp.php
    echo "\$text_color = imagecolorallocate(\$image, 64, 64, 64);" >> /var/www/gifs/gifTemp.php
    echo "imagestring(\$image, 5, 5, 5,  \$text, \$text_color);" >> /var/www/gifs/gifTemp.php
#merge legend on the layer
    echo "imagecopy(\$image, \$image12, 5, 312, 0, 40, 127, 150);" >> /var/www/gifs/gifTemp.php
    echo "imagepng(\$image, 'images/2"$img1"n"$img2"');" >> /var/www/gifs/gifTemp.php
    echo "\$image13 = imagecreatefrompng('images/2"$img1"n"$img2"');" >> /var/www/gifs/gifTemp.php
    echo 'ob_start();' >> /var/www/gifs/gifTemp.php
#transform the png as a gif image
    echo 'imagegif($image13);' >> /var/www/gifs/gifTemp.php
    echo '$frames[]=ob_get_contents();' >> /var/www/gifs/gifTemp.php
#set time between each layer
    echo '$framed[]=40; \nob_end_clean();\n' >> /var/www/gifs/gifTemp.php
  fi;

#for light data, the same
  if [ "$type" = "Lgt" ]; then
    echo '<option value="'$date1' '$date21'">'$date1' '$date21'</option>' >> /var/www/gifs/LgtHist.php

    echo "\$text = \"DateTime : "$date1" at "$date21"\";\n" >> /var/www/gifs/gifLgt.php
    echo "\$image2 = imagecreatefrompng('images/"$img1"n"$img2"');" >> /var/www/gifs/gifLgt.php
    echo "\$image22 = imagecreatefrompng('images/lgtLegend.png');" >> /var/www/gifs/gifLgt.php
    echo "\$text_color = imagecolorallocate(\$image2, 64, 64, 64);" >> /var/www/gifs/gifLgt.php
    echo "imagestring(\$image2, 5, 5, 5,  \$text, \$text_color);" >> /var/www/gifs/gifLgt.php
    echo "imagecopy(\$image2, \$image22, 5, 340, 0, 40, 127, 132);" >> /var/www/gifs/gifLgt.php
    echo "imagepng(\$image2, 'images/2"$img1"n"$img2"');" >> /var/www/gifs/gifLgt.php
    echo "\$image23 = imagecreatefrompng('images/2"$img1"n"$img2"');" >> /var/www/gifs/gifLgt.php
    echo 'ob_start();' >> /var/www/gifs/gifLgt.php
    echo 'imagegif($image23);' >> /var/www/gifs/gifLgt.php
    echo '$frames2[]=ob_get_contents();' >> /var/www/gifs/gifLgt.php
    echo '$framed2[]=40; \nob_end_clean();\n' >> /var/www/gifs/gifLgt.php
  fi;
#for humidity the same again
  if [ "$type" = "Hum" ]; then
    echo '<option value="'$date1' '$date21'">'$date1' '$date21'</option>' >> /var/www/gifs/HumHist.php

    echo "\$text = \"DateTime : "$date1" at "$date21"\";\n" >> /var/www/gifs/gifHum.php
    echo "\$image3 = imagecreatefrompng('images/"$img1"n"$img2"');" >> /var/www/gifs/gifHum.php
    echo "\$image23 = imagecreatefrompng('images/humLegend.png');" >> /var/www/gifs/gifHum.php
    echo "\$text_color = imagecolorallocate(\$image3, 64, 64, 64);" >> /var/www/gifs/gifHum.php
    echo "imagestring(\$image3, 5, 5, 5,  \$text, \$text_color);" >> /var/www/gifs/gifHum.php
    echo "imagecopy(\$image3, \$image23, 5, 340, 0, 40, 127, 132);" >> /var/www/gifs/gifHum.php
    echo "imagepng(\$image3, 'images/2"$img1"n"$img2"');" >> /var/www/gifs/gifHum.php
    echo "\$image33 = imagecreatefrompng('images/2"$img1"n"$img2"');" >> /var/www/gifs/gifHum.php
    echo 'ob_start();' >> /var/www/gifs/gifHum.php
    echo 'imagegif($image33);' >> /var/www/gifs/gifHum.php
    echo '$frames3[]=ob_get_contents();' >> /var/www/gifs/gifHum.php
    echo '$framed3[]=40; \nob_end_clean();\n' >> /var/www/gifs/gifHum.php
  fi;
done < listImg.txt

#end of the selectBox file
echo '</select>\n</form>\n</body>\n</html>' >> /var/www/gifs/TempHist.php
echo '</select>\n</form>\n</body>\n</html>' >> /var/www/gifs/LgtHist.php
echo '</select>\n</form>\n</body>\n</html>' >> /var/www/gifs/HumHist.php

#write function to create animated gif / call the encoder class with usefull parameters
echo "\$gif = new GIFEncoder(\$frames,\$framed,0,2,0,0,0,'bin');\n\$fp = fopen('gif/gifTemp.gif', 'w');\nfwrite(\$fp, \$gif->GetAnimation());\nfclose(\$fp);\n?>" >> /var/www/gifs/gifTemp.php
echo "\$gif = new GIFEncoder(\$frames2,\$framed2,0,2,0,0,0,'bin');\n\$fp = fopen('gif/gifLgt.gif', 'w');\nfwrite(\$fp, \$gif->GetAnimation());\nfclose(\$fp);\n?>" >> /var/www/gifs/gifLgt.php
echo "\$gif = new GIFEncoder(\$frames3,\$framed3,0,2,0,0,0,'bin');\n\$fp = fopen('gif/gifHum.gif', 'w');\nfwrite(\$fp, \$gif->GetAnimation());\nfclose(\$fp);\n?>" >> /var/www/gifs/gifHum.php

#get data from DB to create tables with list of data for each node
psql -U postgres -d Nodes -c "SELECT node_id, temperature, light, humidity from nodes order by node_id;" > '/var/www/gifs/data/'$name1'['$name2
sed '1,2d' '/var/www/gifs/data/'$name1'['$name2  > '/var/www/gifs/data/'$name1'['$name2'EXT'
sed 's/ //g' '/var/www/gifs/data/'$name1'['$name2'EXT' > '/var/www/gifs/data/'$name1'['$name2
sed '/^|/d' '/var/www/gifs/data/'$name1'['$name2 > '/var/www/gifs/data/'$name1'['$name2'EXT'
sed '38,39d' '/var/www/gifs/data/'$name1'['$name2'EXT' > '/var/www/gifs/data/'$name1'['$name2

#create table to display data
echo "<table style=\"position:relative;top:-454;left:512;\">\n<tr><th>ID</th><th>Temperature</th></tr>" > '/var/www/gifs/data/Temp['$name1'['$name2'part1'
echo "<table style=\"position:relative;top:-454;left:512;\">\n<tr><th>ID</th><th>Light</th></tr>" > '/var/www/gifs/data/Lgt['$name1'['$name2'part1'
echo "<table style=\"position:relative;top:-454;left:512;\">\n<tr><th>ID</th><th>Humidity</th></tr>" > '/var/www/gifs/data/Hum['$name1'['$name2'part1'

#second part of the table
echo "<table style=\"position:relative;top:-896;left:665;\">\n<tr><th>ID</th><th>Temperature</th></tr>" > '/var/www/gifs/data/Temp['$name1'['$name2'part2'
echo "<table style=\"position:relative;top:-896;left:665;\">\n<tr><th>ID</th><th>Light</th></tr>" > '/var/www/gifs/data/Lgt['$name1'['$name2'part2'
echo "<table style=\"position:relative;top:-896;left:665;\">\n<tr><th>ID</th><th>Humidity</th></tr>" > '/var/www/gifs/data/Hum['$name1'['$name2'part2'

#for each node (line extracted from query)
#this loop will write each line of the table
while read line
do
#get data
  id=$(echo $line | cut -f1 -d'|')
  temp=$(echo $line | cut -f2 -d'|')
  lgt=$(echo $line | cut -f3 -d'|')
  hum=$(echo $line | cut -f4 -d'|')

#different parts of table
  if [ $id -le 19 ]; then
    part='part1'
  else
    part='part2'
  fi;

#new line into table
  echo "\n<tr align=\"center\"><td>"$id"</td><td>"$temp"</td></tr>" >> '/var/www/gifs/data/Temp['$name1'['$name2$part
  echo "\n<tr align=\"center\"><td>"$id"</td><td>"$lgt"</td></tr>" >> '/var/www/gifs/data/Lgt['$name1'['$name2$part
  echo "\n<tr align=\"center\"><td>"$id"</td><td>"$hum"</td></tr>" >> '/var/www/gifs/data/Hum['$name1'['$name2$part
done < '/var/www/gifs/data/'$name1'['$name2

#end of tables
echo "</table>" >> '/var/www/gifs/data/Temp['$name1'['$name2'part1'
echo "</table>" >> '/var/www/gifs/data/Lgt['$name1'['$name2'part1'
echo "</table>" >> '/var/www/gifs/data/Hum['$name1'['$name2'part1'

echo "</table>" >> '/var/www/gifs/data/Temp['$name1'['$name2'part2'
echo "</table>" >> '/var/www/gifs/data/Lgt['$name1'['$name2'part2'
echo "</table>" >> '/var/www/gifs/data/Hum['$name1'['$name2'part2'
fi;
