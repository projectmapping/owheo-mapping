#!/usr/bin/env bash

#################################
# This script deploys GeoServer #
#################################

#copying web archive into webapps folder for autodeployment
sudo cp /vagrant/geoserver.war /var/lib/tomcat7/webapps/

#restarting tomcat to deploy geoserver
sudo /etc/init.d/tomcat7 restart

#enabling WPS service for Heatmap transformation
#This is necessary to transform nodes points into rasters
sudo cp /vagrant/geoserver-2-3/* /var/lib/tomcat7/webapps/geoserver/WEB-INF/lib/

#restarting tomcat to enable WPS service
sudo /etc/init.d/tomcat7 restart
