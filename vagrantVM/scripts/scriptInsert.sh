#! /bin/bash

##########################################################################################################
# This script writes and executes queries to insert data into tables from the file sensors.csv on the host #
##########################################################################################################

#if the file exists start the script
if [ -f /vagrant/sensors.csv ]; then
#set the source file
source=/vagrant/sensors.csv

echo -e "\n\033[31m EXTRACTING FROM FILE\033[32m"

#extracting columns to insert
cut -f1,3,4,6,8 -d',' $source  > tempData.txt
#delete first line (columns name / starting with m)
sed '/^[m*]/d' tempData.txt > tempData2.txt
rm tempData.txt

#init new line for loop
oldIFS=$IFS
IFS=$'\n'

echo -e "\n\033[31mWRITTING QUERIES...\n\033[32m"

#query counter
i=0
while read line
do
    i=$((i+1))
#this is the node_id
    node=$(echo $line | cut -f1 -d',')
#and this one is the temperature
    temp=$(echo $line | cut -f2 -d',')
#and now the lightness
    lgt=$(echo $line | cut -f3 -d',')
#aaaaaaand humidity
    hum=$(echo $line | cut -f4 -d',')
#timestamp
    time0=$(echo $line | cut -f5 -d',')
    time=$(echo $time0 | cut -f1 -d'.')
    time1=$(echo $time | cut -f1 -d' ')
    time2=$(echo $time | cut -f2 -d' ')
    time3=$(echo $time1'T'$time2'.0Z')

#writting queries for inserting into Nodes DB
    echo "psql -U postgres -d Nodes -c \"UPDATE nodes SET temperature = "$temp", light = "$lgt", humidity = "$hum", timestamp = '"$time3"' WHERE node_id = "$node";\"" >> /vagrant/insert.txt
    echo 'query n.'$i
done < tempData2.txt

echo -e "\n\033[31mALL QUERIES WRITTEN"
echo -e "INSERTING DATA...\n\033[32m"
sudo rm tempData2.txt

#update sensors table data
sudo sh /vagrant/insert.txt
sudo rm /vagrant/insert.txt

#display some stuff
echo -e "\n\033[31mALL DATA INSERTED"
echo -e "\033[31mLAYERS UP TO DATE"

#call script to create gifs
sudo sh /vagrant/scripts/scriptGif.sh

#delete the file
sudo rm /vagrant/sensors.csv

echo -e "\n\033[31mREADY\n\033[32m"
fi;
