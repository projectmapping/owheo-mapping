WHAT ARE THESE FILES ??????

--VagrantFile : config file used to up the VM, commands used on the guest
|
|
--engifs : file copied on apache2 repositories to enable site
|
|
--geoserver-2-3 : library for WPS, enables heatmap rendering
|    |
|    --gs-web-wps-2.5.jar
|    |
|    --gs-wps-core=2.5.jar
|    |
|    --gt-process-geometry-11.0.jar
|    |
|    --gt-xsd-wps-11.0.jar
|    |
|    --net.opengis.wps-11.0.jar
|    |
|    --serializer-2.7.1.jar
|
|
--geoserver.war : web archive of geoserver
|
|
--gifs : contains app to display gifs
|    |
|    --GIFEncoder.class.php : encode file into gif format
|    |
|    --error : contains image to display when no gif available
|    |  |
|    |  --404.png : cute cat appears when no image available
|    |
|    --index.html : index page calling php scripts & gifs
|    |
|    --testgif : conf file of the app
|
|
--osmdb : contains osm files, osm2pgsql styles and script to rewrite osm files from JOSM
|    |
|    --Nodes.osm : nodes localisation with level and node_id tags
|    |
|    --Owheo.osm : map of the 3 floors Owheo building with tags
|    |
|    --default.style : osm2pgsql style for Owheo.osm
|    |
|    --defaultNode.style : osm2pgsql style for Nodes.osm
|    |
|    --script.sh : converts JOSM OSM file to make them readable by osm2pgsql USE : ./script.sh fileToConvert.osm newFile.osm	NOT USED
|
|
--pg_hba.conf : postgreSQL client authentification, this file modified enables connection from the host machine
|
|
--scripts : contains scripts to configure VM
|    |
|    --scriptApache2.sh : install apache2, configure php.conf for html parsing and enable gifs site
|    |
|    --scriptGif.sh : create php scripts to create gifs and capture png from geoserver
|    |
|    --confGeo.sh : configure GeoServer
|    |
|    --instTomcat.sh : install OpenJDK and tomcat7
|    |
|    --scriptDB.sh : create DBs with extensions, insert OSM files data and create fake node
|    |
|    --scriptLoop.sh : infinite loop checking if sensors.csv exists
|    |    
|    --scriptInsert.sh : writes queries and insert data into tables
|    |
|    --scriptSensorsLayer.sh : add useful folders in gifs app, update GeoServer to add heatmap layer and grouplayers
|    |
|    --scriptUpdate.sh : writes file with queries to update sensors table with nodes table data
|    |
|    --instGeo.sh : copies geoserver.war into tomcat webapps folder and deploy it
|    |
|    --instPsql.sh : install postgreSQL, GEOS, postGIS and osm2pgsql
|
|
--stores : contains styles files, layers files to create layers and heatmap styles
|    |
|    --LayerGroupHumidityNow.xml : description of humidity group layer with newest data
|    |
|    --LayerGroupLightNow.xml : description of light group layer with newest data
|    |
|    --LayerGroupTemperatureNow.xml : description of temperature group layer with newest data
|    |
|    --NodeHumidityStyle.sld : style file for humidity group layer
|    |
|    --NodeHumidityStyle.xml : description of style sld file
|    |
|    --NodeLightStyle.sld : style file for light group layer
|    |
|    --NodeLightStyle.xml : description of style sld file
|    |
|    --NodeStyle.sld : style file for temperature group layer
|    |
|    --NodeStyle.xml : description of style sld file
|    |
|    --OwheoStyle.sld : style for Owheo building layer
|    |
|    --storeNodes.xml : description of Nodes store to create
|    |
|    --storeOwheo.xml : description of Owheo store to create
|
--tomcat-users.xml : file containing user authentification for tomcat
